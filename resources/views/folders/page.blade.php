@extends('layouts/main')

@section('title', '- Courses')

@section('styles')

    {{Html::style('css/styles.css')}}
    
@endsection

@section('content')

<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="text-center">
            <h1 class="all-form-group">Bachelor</h1>
            <hr class="create-hr-upper">
            @foreach ($bachelors as $bachelor)
            <div class="text-center category-box course-year">
                <a href="{{route('folders.index', $bachelor->id)}}" class="all-form-group categories-button">{{$bachelor->year}}</a><br>
            </div>
            @endforeach
        </div>
    </div>
    <div class="col-md-6 mx-auto">
        <div class="text-center">
            <h1 class="all-form-group">Magister</h1>
            <hr class="create-hr-upper">
            @foreach ($magisters as $magister)
            <div class="text-center category-box course-year">
                <a href="{{route('folders.index', $magister->id)}}" class="all-form-group categories-button">{{$magister->year}}</a><br>
            </div>
            @endforeach
        </div>
    </div>
</div>

@endsection

@section('scripts')

    {{Html::script('js/parsley.min.js')}}

@endsection