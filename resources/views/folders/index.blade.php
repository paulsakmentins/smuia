@extends('layouts/main')

@section('title', '- Folders')

@section('styles')

    {{Html::style('css/styles.css')}}
    
@endsection

@section('content')

    <div class="input-container input-container-posts">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-6">
                <h1 class="all-form-group">{{$program->name}} - {{$program->year}}</h1>
            </div>
            <div class="col-md-4 col-sm-4 col-6 top-button">
                <a href="{{route('folders.create', $program->id)}}" class="submit-button posts-ask-question-button">Add folder</a>
            </div>
            <div class="col-md-12">
                <hr class="create-hr-upper">
            </div>
        </div>
        @if($folders != null)
        <div class="row">
            @foreach ($folders->where('program_id', $program->id) as $folder)
                <div class="col-md-6 col-sm-6 col-xs-12 mx-auto">
                    <div class="text-center folders">
                        <a href="{{route('folders.show', $folder->id)}}" class="all-form-group categories-button">{{$folder->name}}</a><br>
                    </div>
                </div>
            @endforeach
        </div>
        @endif
@endsection

@section('scripts')

    {{Html::script('js/parsley.min.js')}}

@endsection