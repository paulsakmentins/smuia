@extends('layouts/main')

@section('title', '- Folder')

@section('styles')

    {{Html::style('css/styles.css')}}
    
@endsection

@section('content')
    <div class="input-container input-container-posts">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-8">
                <h1 class="all-form-group">{{$folder->name}}</h1>
                <p class="all-form-group folder-author">Created by {{$folder->user->name}} {{$folder->user->surname}}</p>
            </div>
            <div class="col-md-4 col-sm-4 col-4 top-button">
            @if((Auth::user()->id == $folder->user->id) || (Auth::user()->isAdmin == 1))
                {{ Form::open(['route' => ['folders.destroy', $folder->id], 'method' => 'DELETE']) }}
                    {{Form::submit('Delete', ['class' => 'submit-button html-button folder-action-buttons'])}}
                {{ Form::close() }}
                <a href="{{route('folders.edit', $folder->id)}}" class="submit-button html-button html-button-left folder-action-buttons">edit</a>
            @endif
            </div>
            <div class="col-md-12">
                <hr class="create-hr-upper">
            </div>
        </div>
    @if($files != null)
    <div class="row">
        @foreach ($files->where('folder_id', $folder->id) as $file)
            <div class="col-md-4 col-sm-6 col-xs-12 mx-auto">
                <div class="text-center folders">
                    <a href="{{route('files.show', $file->id)}}" class="all-form-group categories-button">{{$file->name}}</a><br>
                    <object data="{{asset('folder_files/' . $file->file)}}" width="100%" height="240px"></object>
                </div>
            </div>
        @endforeach
    </div>
    @endif
    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12 mx-auto">
            <div class="text-center">
                <a href="{{route('files.create', $folder->id)}}" class="all-form-group add-plus">+</a>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    {{Html::script('js/parsley.min.js')}}

@endsection