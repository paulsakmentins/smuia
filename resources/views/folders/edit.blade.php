@extends('layouts/main')

@section('title', '- Edit folder')

@section('styles')

    {{Html::style('css/parsley.css')}}
    {{Html::style('css/styles.css')}}

@endsection

@section('content')
    <div class="row">
        <div class="offset-md-2 col-md-8 input-container">
            <h1 class="all-form-group">Edit the folder</h1>
            <hr class="create-hr-upper">
            {{ Form::model($folder, ['route' => ['folders.update', $folder->id], 'data-parsley-validate' => '', 'method' => 'POST']) }}
                {{Form::label('name', 'Folder name', ['class' => 'all-form-group'])}}
                {{Form::text('name', null, array('class' => 'form-control form-group-pages', 'required' => '', 'maxlength' => '30', 'minlength' => '5'))}}

            <hr class="create-hr">
                {{Html::linkRoute('folders.show', 'Cancel', array($folder->id), array('class' => 'submit-button html-button'))}}                    
                {{Form::submit('Update', ['class' => 'submit-button html-button html-button-left'])}}
            {{ Form::close() }}
        </div>
    </div>

@endsection

@section('scripts')

    {{Html::script('js/parsley.min.js')}}

@endsection