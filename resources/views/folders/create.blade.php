@extends('layouts/main')

@section('title', '- Add folder')

@section('styles')

    {{Html::style('css/parsley.css')}}
    {{Html::style('css/styles.css')}}

@endsection

@section('content')
    <div class="row">
        <div class="offset-md-2 col-md-8 input-container">
            <h1 class="all-form-group">Create a folder</h1>
            <hr class="create-hr-upper">
            {{ Form::open(['route' => ['folders.store', $program->id], 'data-parsley-validate' => '', 'method' => 'POST']) }}

                {{Form::label('name', 'Folder name', ['class' => 'all-form-group'])}}
                {{Form::text('name', null, array('class' => 'form-control form-group-pages', 'required' => '', 'maxlength' => '30', 'minlength' => '5'))}}

            <hr class="create-hr">
                {{Form::submit('Add folder', array('class' => 'submit-button' ))}}
            {{ Form::close() }}
        </div>
    </div>

@endsection

@section('scripts')

    {{Html::script('js/parsley.min.js')}}

@endsection