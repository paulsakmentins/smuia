<!DOCTYPE html>
<html>
<head>
    <title>Welcome to SMUIA</title>
</head>

<body>
<div>
    <p style="color: #8AAAE5;">{{$subject}}</p>
</div>
<div>
    <p style="color: #8AAAE5;">{{$body}}</p>
</div>
<div>
    <p style="color: #8AAAE5;">Sent by: {{$email}}</p>
    <hr style="border-color: #8AAAE5; margin-top: 50px;">
</div>
<body>
</html>