<!DOCTYPE html>
<html>
<head>
    <title>Welcome to SMUIA</title>
</head>

<body>
<div>
    <h1 style="color: #8AAAE5;">Welcome to the University of Latvia students learning website</h1>
</div>
<div>
    <p style="color: #8AAAE5;">Please verify your e-mail by clicking on the button below</p>
    <br>
    <div style="text-align: center;">
        <a href="{{url('user/verify', $user->verifyUser->token)}}" style="background:none; border:none; text-decoration: none; color: #8AAAE5; text-transform: lowercase; border: 1px solid #8AAAE5; padding: 20px;">Verify account</a>
    </div>
    <hr style="border-color: #8AAAE5; margin-top: 50px;">
</div>
<body>
</html>