<nav class="navbar navigation-bar navbar-expand-md">
<a class="navbar-brand navigation-links" href="/">SMUIA</a>
  <button class="navbar-toggler navigation-links navbar-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            @if (Auth::check())
            <li class="nav-item {{Request::is('/') ? "active" : ""}}">
                <a class="nav-link navigation-links" href="/questions">Questions<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item {{Request::is('/') ? "active" : ""}}">
                <a class="nav-link navigation-links" href="/archive">Files<span class="sr-only">(current)</span></a>
            </li>
            @else
            <li class="nav-item {{Request::is('about') ? "active" : ""}}">
                <a class="nav-link navigation-links" href="/about">About</a>
            </li>
            <li class="nav-item {{Request::is('contact') ? "active" : ""}}">
                <a class="nav-link navigation-links" href="/contact">Contact</a>
            </li>
            @endif
        </ul>
        <ul class="navbar-nav navbar-right ml-auto">
            @if (Auth::check())
                <div class="profile">
                    @if(Auth::user()->image)
                        <img src="{{asset('profile_images/' . Auth::user()->image)}}" class="profile-image profile-picture-img" width="150px" height="150px">
                    @else
                        <img src="{{asset('default_images/profile.png')}}" class="profile-image profile-picture-img" width="150px" height="150px">
                    @endif
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle navigation-links navbar-right capitalize profile-name-tag" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{Auth::user()->name}} {{Auth::user()->surname}}
                </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="my-account nav-link" href="{{route('account.profile')}}">My profile</a>
                @if(auth()->user()->isAdmin == 1)
                <a class="my-account nav-link" href="{{route('account.users')}}">Manage users</a>
                @endif
                <hr class="create-hr create-hr-pages">
                <a class="my-account nav-link" href="{{route('logout')}}">Log out</a>
            </div>
            </div>
            </li>
            @else
              <a href="{{route('register')}}" class="navigation-links navbar-right register-link">Register</a>
              <a href="{{route('login')}}" class="navigation-links navbar-right login-link">Login</a>
            @endif
        </ul>
    </div>
</nav>
