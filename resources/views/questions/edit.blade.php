@extends('layouts/main')

@section('title', '- Edit question')

@section('styles')

    {{Html::style('css/parsley.css')}}
    {{Html::style('css/styles.css')}}

@endsection

@section('content')
    <div class="row">
        <div class="offset-md-2 col-md-8 input-container data-parsley-validate">
            <h1 class="all-form-group">Edit your question</h1>
            <div class="date-container">
                <p class="date-title-style">last modified: </p>
                <p class="date-style">{{date('M j, Y', strtotime($post -> updated_at))}}</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="offset-md-2 col-md-8 input-container data-parsley-validate">
            <hr class="create-hr-upper">
                {{Form::model($post, ['route' => ['questions.update', $post->id], 'method' => 'PATCH', 'files' => true, 'data-parsley-validate' => ''])}}
                        {{Form::label('title', 'Title', ["class" => "all-form-group"])}}
                        {{Form::text('title', null, ["class" => 'form-control form-group-pages', 'required' => '', 'maxlength' => '255'])}}
                        
                        {{Form::label('category_id', 'Category', ['class' => 'all-form-group'])}}
                        <select class="form-control form-group-pages btn register-dropdown dropdown-toggle" name="category_id">
                            @foreach ($categories as $category)
                                <option value='{{$category->id}}'>{{$category->name}}</option>
                            @endforeach
                        </select>
                        
                        {{Form::label('body', 'Description', ["class" => "all-form-group"])}}
                        {{Form::textarea('body', null, ["class" => 'form-control form-group-pages', 'required' => ''])}}
                        
                        {{Form::label('image', 'Upload image', ['class' => 'all-form-group'])}}
                        <br>
                        {{Form::file('image', null, array('class' => 'form-control form-group-pages'))}}
                        
                        <hr class="create-hr">
                    <div style="button-container">
                        {{Html::linkRoute('questions.show', 'Cancel', array($post->id), array('class' => 'submit-button html-button'))}}                    
                        {{Form::submit('Update', ['class' => 'submit-button html-button html-button-left'])}}
                    </div>
                {{Form::close()}}
        </div>
    </div>
@endsection

@section('scripts')

    {{Html::script('js/parsley.min.js')}}

@endsection