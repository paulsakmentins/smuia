@extends('layouts/main')

@section('title', '- View question')

@section('styles')

    {{Html::style('css/styles.css')}}
    
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-sm-8 col-8 input-container">
            <h1 class="all-form-group">{{$post->title}}</h1>
            <p class="all-form-group">Author: 
            @if(auth()->user()->id != $post->user->id)
                <a href="{{route('account.show', $post->user->id)}}" class="profile-href folder-author">{{$post->user->name}} {{$post->user->surname}}</a></p>
            @else  
                <a href="{{route('account.profile')}}" class="profile-href folder-author">{{$post->user->name}} {{$post->user->surname}}</a></p>
            @endif
        </div>
        <div class="col-md-4 col-sm-4 col-4 top-button">
            {{Html::linkRoute('questions.index', 'all questions', null, ["class" => "submit-button posts-ask-question-button folder-action-buttons folder-action-buttons-questions"])}}
        </div>
    </div>
    <div class="row"> 
        <div class="col-md-12">
            <hr class="create-hr-upper">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p class="all-form-group">{{$post->body}}</p>
            @if($post->image != NULL)
            <img src="{{asset('post_images/' . $post->image)}}" class="img-fluid"/>
            @endif
            <hr class="create-hr">
            @if((Auth::user()->id == $post->user_id) || (Auth::user()->isAdmin == 1))
            <div class="button-container">
                <div class="show-button-container-edit">
                    {{Html::linkRoute('questions.edit', 'Edit', array($post -> id), array('class' => 'submit-button html-button html-button-left'))}}
                </div>
                <div class="show-button-container-delete">
                    {{Form::open(['route'=>['questions.destroy', $post -> id], 'method' => 'DELETE'])}}
                    {{Form::submit('Delete', ['class' => 'submit-button html-button'])}}
                    {{Form::close()}}
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        @if ($post->comments()->count() === 1) 
            <h3 class="all-form-group">{{$post->comments()->count()}} comment</h3>
        @else 
            <h3 class="all-form-group">{{$post->comments()->count()}} comments</h3>
        @endif
            @foreach($post->comments as $comment)
            <div class="comment">
                <div class="author">
                @if($comment->user->image)
                    <img src="{{asset('profile_images/' . $comment->user->image)}}" class="author-image">
                @else
                    <img src="{{asset('default_images/profile.png')}}" class="author-image">
                @endif
                </div>
                <div class="author-name">
                @if(auth()->user()->id != $comment->user->id)
                    <a href="{{route('account.show', $comment->user->id)}}" class="profile-href">{{$comment->user->name}} {{$comment->user->surname}}</a></p>
                @else  
                    <a href="{{route('account.profile')}}" class="profile-href">{{$comment->user->name}} {{$comment->user->surname}}</a></p>
                @endif
                    <p class="date-style">{{date('M j, Y', strtotime($comment->created_at))}}</p>
                </div>
                <div class="comment-text">
                    <p>{{$comment->comment}}</p>
                </div>
            </div>
            <hr class="create-hr">
            @if((Auth::user()->id == $comment->user->id) || (Auth::user()->isAdmin == 1))
                <div class="button-container">
                    <div class="show-button-container-edit">
                        {{Html::linkRoute('comments.edit', 'Edit', array($comment->id), array('class' => 'submit-button html-button html-button-left'))}}
                    </div>
                    <div class="show-button-container-delete">
                        {{ Form::open(['route' => ['comments.destroy', $comment -> id], 'method' => 'DELETE']) }}
                            {{Form::submit('Delete', ['class' => 'submit-button html-button html-button-left'])}}
                        {{ Form::close() }}
                    </div>
                </div>
            @endif
            @endforeach
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="comment-form">
                {{Form::open(['route' => ['comments.store', $post->id], 'method' => 'POST'])}}
                    {{Form::textarea('comment', null, ['class' => 'form-control form-group-pages', 'rows' => '3'])}}
                    {{Form::submit('Add comment', ['class' => 'submit-button html-button'])}}
                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    {{Html::script('js/parsley.min.js')}}
    {{Html::script('js/script.js')}}

@endsection