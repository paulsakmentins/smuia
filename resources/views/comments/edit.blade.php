@extends('layouts/main')

@section('title', '- Edit comment')

@section('styles')

    {{Html::style('css/parsley.css')}}
    {{Html::style('css/styles.css')}}
    
@endsection

@section('content')
    <div class="row">
        <div class="offset-md-2 col-md-8 input-container data-parsley-validate">
            <h1 class="all-form-group">Edit your comment</h1>
            <div class="date-container">
                <p class="date-title-style">last modified: </p>
                <p class="date-style">{{date('M j, Y', strtotime($comment -> updated_at))}}</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="offset-md-2 col-md-8 input-container data-parsley-validate">
            <hr class="create-hr-upper">
                {{Form::model($comment, ['route' => ['comments.update', $comment->id], 'method' => 'PUT', 'data-parsley-validate' => ''])}}
                        
                        {{Form::label('comment', ' ', ["class" => "all-form-group"])}}
                        {{Form::textarea('comment', null, ["class" => 'form-control form-group-pages', 'required' => ''])}}
                        <hr class="create-hr">
                    <div style="button-container">                    
                        {{Html::linkRoute('questions.show', 'Cancel', array($comment->post->id), array('class' => 'submit-button html-button'))}}
                        {{Form::submit('Update', ['class' => 'submit-button html-button html-button-left'])}}
                    </div>
                {{Form::close()}}
        </div>
    </div>

@section('scripts')

    {{Html::script('js/parsley.min.js')}}

@endsection

@endsection