@extends('layouts/main')

@section('title', "- Login")

@section('styles')

    {{Html::style('css/parsley.css')}}
    {{Html::style('css/styles.css')}}
    
@endsection

@section('content')
<div class="offset-md-3 col-md-6">
    <div class="text-center">
        <img src="{{asset('default_images/logo.jpg')}}" class="img-fluid" width="150px"/>
        <h1 class="all-form-group">Welcome to SMUIA</h1>
    </div>
    <hr class="create-hr-upper">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    @if (session('verified'))
    <div class="alert alert-warning">
        {{ session('verified') }} 
        <a href="{{route('login.verify')}}" class="resend-button">resend activation link</a>
    </div>
    @endif
    @if (session('danger'))
    <div class="alert alert-danger">
        {{ session('danger') }}
    </div>
    @endif
    {{Form::open(['data-parsley-validate' => ""])}}
        {{Form::label('email', 'E-mail', ['class' => 'all-form-group'])}}
        {{Form::email('email', null, ['class' => 'form-control form-group-pages', 'required' => '', 'pattern' => '.*@.*lu.lv$'])}}

        {{Form::label('password', 'Password', ['class' => 'all-form-group'])}}
        {{Form::password('password', ['class' => 'form-control form-group-pages', 'required' => ''])}}

        <hr class="create-hr">
        {{Form::submit('Login', ['class' => 'submit-button html-button html-button-left'])}}
        
        <p><a href="{{url('password/reset')}}" class="submit-button html-button html-button-left">Forgot password</a>
    {{Form::close()}}
</div>
@endsection

@section('scripts')

    {{Html::script('js/parsley.min.js')}}

@endsection