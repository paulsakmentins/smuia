@extends('layouts\main')

@section('title', '- All questions')

@section('styles')

    {{Html::style('css/styles.css')}}
    
@endsection

@section('content')
    <div class="input-container">
        <div class="row">
            <div class="offset-md-2 col-md-8">
                @if($user->image)
                    <img src="{{asset('profile_images/' . $user->image)}}" class="profile-picture-img" width="150px" height="150px">
                @else
                    <img src="{{asset('default_images/profile.png')}}" class="profile-picture-img" width="150px" height="150px">
                @endif
                <h1 class="all-form-group profile-page-name">{{$user->name}} {{$user->surname}}</h1>
                <div class="row">
                    <div class="col-md-4">
                        <p class="all-form-group"><strong>Program</strong></p>
                        <p class="all-form-group">{{$user->program}}</p>
                    </div>
                    <div class="col-md-4">
                        <p class="all-form-group"><strong>Year</strong></p>
                        <p class="all-form-group">{{$user->year}}</p>
                    </div>
                    <div class="col-md-4">
                        <p class="all-form-group"><strong>Email</strong></p>
                        <p class="all-form-group">{{$user->email}}</p>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')

    {{Html::script('js/parsley.min.js')}}

@endsection