@extends('layouts\main')

@section('title', '- All questions')

@section('styles')

    {{Html::style('css/styles.css')}}
    
@endsection

@section('content')
        <div class="row">
            <div class="col-md-6 col-sm-6 col-5 mx-auto">
            <div class="text-center">
                @if(auth()->user()->image)
                    <img src="profile_images/{{auth()->user()->image}}" class="profile-picture-img img-fluid">
                @else
                    <img src="{{asset('default_images/profile.png')}}" class="profile-picture-img img-fluid" width="150px" height="150px">
                @endif
            </div>
            </div>
            <div class="col-md-6 col-sm-6 col-7 mx-auto">
                <h1 class="all-form-group">{{auth()->user()->name}} {{auth()->user()->surname}}</h1>
                <p class="all-form-group"><strong>Program</strong></p>
                <p class="all-form-group">{{auth()->user()->program}}</p>
                <p class="all-form-group"><strong>Year</strong></p>
                <p class="all-form-group">{{auth()->user()->year}}</p>
                <p class="all-form-group"><strong>Email</strong></p>
                <p class="all-form-group">{{auth()->user()->email}}</p>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="offset-md-2 col-md-8">
                {{Form::model($user, ['route' => ['profile.update'], 'method' => 'POST', 'files' => true, 'data-parsley-validate' => ''])}}
                    {{Form::label('name', 'Name', ['class' => 'all-form-group'])}}
                    {{Form::text('name', auth()->user()->name, ['class' => 'form-control form-group-pages', 'required' => ''])}}

                    {{Form::label('surname', 'Surname', ['class' => 'all-form-group'])}}
                    {{Form::text('surname', auth()->user()->surname, ['class' => 'form-control form-group-pages'])}}

                    <div class="row">
                        <div class="col-md-6">
                            {{Form::label('program', 'Program', ['class' => 'all-form-group'])}}
                            {{Form::select('program', ['Bachelor' => 'Bachelor', 'Magister' => 'Magister', 'Lecturer' => 'Lecturer'], old('program', auth()->user()->program), ['class' => 'form-control form-group-pages btn register-dropdown dropdown-toggle', 'required' => '', 'id' => 'program'])}}
                        </div>
                        <div class="col-md-6">
                            {{Form::label('year', 'Year', ['class' => 'all-form-group'])}}
                            {{Form::select('year', ['1 year' => '1 year', '2 year' => '2 year', '3 year' => '3 year', '4 year' => '4 year'], old('year', auth()->user()->year), ['class' => 'form-control form-group-pages btn register-dropdown dropdown-toggle', 'id' => 'year'])}}
                        </div>
                    </div>
                    
                    {{Form::label('image', 'Upload image', ['class' => 'all-form-group'])}}
                    <br>
                    {{Form::file('image', null, array('class' => 'form-control form-group-pages'))}}
            <hr class="create-hr">
                {{Form::submit('Update profile', array('class' => 'submit-button' ))}}
            {{ Form::close() }}
            </div>
@endsection

@section('scripts')

    {{Html::script('js/parsley.min.js')}}
    {{Html::script('js/script.js')}}

@endsection