@extends('layouts\main')

@section('title', '- All questions')

@section('styles')

    {{Html::style('css/styles.css')}}
    
@endsection

@section('content')

    <div class="input-container input-container-posts">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-8">
                <h1 class="all-form-group headline">All users</h1>
            </div>
            <div class="col-md-12">
                <hr class="create-hr-upper">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table all-form-group">
                    <thead class="thead-styling">
                        <th class="table-category">Image</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th class="table-time">Program</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td class="table-styling table-category">
                                    @if($user->image)
                                        <img src="{{asset('profile_images/' . $user->image)}}" class="author-image">
                                    @else
                                        <img src="{{asset('default_images/profile.png')}}" class="author-image">
                                    @endif
                                </td>
                                <td class="table-styling">{{$user->name}}</td>
                                <td class="table-styling">{{$user->surname}}</td>
                                <td class="table-time" style="text-transform: lowercase; table-styling">{{$user->program}}</td>
                                <td>
                                    <a href="{{route('account.user', $user->id)}}" class="submit-button html-button view-button">Manage</a>
                                </td>
                            </tr>  
                        @endforeach
                    </tbody>
                </table>
                <div class="pagination">
                    {{$users -> links("pagination::bootstrap-4"), ['class' => 'pagination']}}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    {{Html::script('js/parsley.min.js')}}

@endsection