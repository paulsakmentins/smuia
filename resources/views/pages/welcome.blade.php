@extends('layouts/main')

@section('title', "- Home")

@section('styles')

    {{Html::style('css/styles.css')}}
    
@endsection


@section('content')     
<div class="welcome-questions">
    <div class="row">
        <div class="col-md-12">
            <h1 class="all-form-group">Questions section</h1>
            <hr class="create-hr-upper">
            <p class="all-form-group">
            Questions section is meant for students and lecturers to share their
            knowledge, help each other and learn by investing their time by helping
            and solving problems, that have been added to the section.<br> Questions section
            includes 3 categories - homework, programming, lecture, where in each of those
            categories user is able to create a question, describing the problem by text 
            and image. <br>Each question has comments section, where every user can leave a comment
            with possible solution for the given question. 
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 class="all-form-group">Files section</h1>
            <hr class="create-hr-upper">
            <p class="all-form-group">
            Files section allows users to create folders and store files in them. There are 2 
            programs - Bachelor and Magister, where under them are courses. <br>Once user goes to
            the needed course, it is possible to create folders and add files - image or pdf, to those folders.
            <br>For example, user can make folder <i>Operation systems</i> and in there add files, such as homeworks, 
            tests, some images from the class board.
            </p>
        </div>
    </div>
</div>

    
@endsection