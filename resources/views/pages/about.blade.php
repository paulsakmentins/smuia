@extends('layouts/main')

@section('title', "- About")

@section('styles')

    {{Html::style('css/styles.css')}}
    
@endsection

@section('content')    
<div class="row">
    <div class="col-md-12">
        <h1 class="all-form-group">About</h1>
        <hr class="create-hr-upper">
    </div>
</div>

<div class="about-page">
    <div class="row">
        <div class="col-md-12">
            <h1 class="all-form-group">What is SMUIA?</h1>
            <p class="all-form-group welcome-text">
            SMUIA is a learning and information exchange platform for students, which creation 
            was inspired by <i>stackoverflow.com</i> and <i>Google Drive</i>.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 class="all-form-group">Purpose of SMUIA?</h1>
            <p class="all-form-group welcome-text">
            The main goal is to solve problems, which exists in current situation. <br>
            For example, computer science student's main source for asking questions
            is in course's <i>WhatsApp</i> group, where they are not properly viewable,
            it is hard to discuss them and with 200 people chat groups, they disappear
            in really short amount of time.<br>Also, we have developed tradition by making 
            a files archive with old lecture materials, tests and homeworks, where everyone
            can access, view and edit those file submissons. This means, that anyone can just
            delete someone's added file or folder without needing any permissions.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 class="all-form-group">Who can use SMUIA?</h1>
            <p class="all-form-group welcome-text">
            This website can be used by everyone, that has access to 
            <i>https://webmails.lu.lv/</i>and can sign in to student's 
            e-mail account, which format is like this - example<strong>@lu.lv</strong>. 
            Person should register with real Univeristy of Latvia e-mail
            account, because there will be activation link sent to the e-mail person has
            registered with.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 class="all-form-group">What does SMUIA offer?</h1>
            <p class="all-form-group welcome-text">
            SMUIA website includes 2 sections - questions and files. <br>
            In questions section user can ask questions about things, that
            concludes topics about homeworks, lectures and programming tasks.<br>
            In files section user can choose program - Bachelor or Magister, course year, 
            according to program and afterwards add folders and files in them, to make
            a file archive for each course.
            </p>
        </div>
    </div>
</div>
@endsection