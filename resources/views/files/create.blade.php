@extends('layouts/main')

@section('title', '- Add file')

@section('styles')

    {{Html::style('css/parsley.css')}}
    {{Html::style('css/styles.css')}}

@endsection

@section('content')

    <div class="row">
        <div class="offset-md-2 col-md-8 input-container">
            <h1 class="all-form-group">Add a file</h1>
            <hr class="create-hr-upper">
            {{ Form::open(['route' => ['files.store', $folder->id], 'data-parsley-validate' => '', 'files' => true]) }}

                {{Form::label('name', 'Name', ['class' => 'all-form-group'])}}
                {{Form::text('name', null, array('class' => 'form-control form-group-pages', 'required' => '', 'maxlength' => '255'))}}

                {{Form::label('file', 'Upload file', ['class' => 'all-form-group'])}}
                <br>
                {{Form::file('file', null, array('class' => 'form-control form-group-pages'))}}

            <hr class="create-hr">
                {{Form::submit('Upload file', array('class' => 'submit-button' ))}}
            {{ Form::close() }}
        </div>
    </div>

@endsection

@section('scripts')

    {{Html::script('js/parsley.min.js')}}

@endsection