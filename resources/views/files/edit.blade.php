@extends('layouts/main')

@section('title', '- Edii folder')

@section('styles')

    {{Html::style('css/parsley.css')}}
    {{Html::style('css/styles.css')}}

@endsection

@section('content')
    <div class="row">
        <div class="offset-md-2 col-md-8 input-container">
            <h1 class="all-form-group">Edit the file</h1>
            <hr class="create-hr-upper">
            {{ Form::model($file, ['route' => ['files.update', $file->id], 'data-parsley-validate' => '', 'method' => 'POST']) }}
                {{Form::label('name', 'File name', ['class' => 'all-form-group'])}}
                {{Form::text('name', null, array('class' => 'form-control form-group-pages', 'required' => '', 'maxlength' => '30', 'minlength' => '2'))}}

                {{Form::label('file', 'Upload image', ['class' => 'all-form-group'])}}
                <br>
                {{Form::file('file', null, array('class' => 'form-control form-group-pages'))}}
            
            
            <hr class="create-hr">
                {{Html::linkRoute('files.show', 'Cancel', array($file->id), array('class' => 'submit-button html-button'))}}                    
                {{Form::submit('Update', ['class' => 'submit-button html-button html-button-left'])}}
            {{ Form::close() }}
        </div>
    </div>

@endsection

@section('scripts')

    {{Html::script('js/parsley.min.js')}}

@endsection