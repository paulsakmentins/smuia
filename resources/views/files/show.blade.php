@extends('layouts/main')

@section('title', '- Files')

@section('styles')

    {{Html::style('css/styles.css')}}
    
@endsection

@section('content')
    <div class="input-container input-container-posts">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-8">
                <h1 class="all-form-group">{{$file->name}}</h1>
                <p class="all-form-group folder-author">Created by {{$file->user->name}} {{$file->user->surname}}</p>
            </div>
            <div class="col-md-4 col-sm-4 col-4 top-button">
            @if((Auth::user()->id == $file->user->id) || (Auth::user()->isAdmin == 1))
                {{ Form::open(['route' => ['files.destroy', $file->id], 'method' => 'DELETE']) }}
                    {{Form::submit('Delete', ['class' => 'submit-button html-button folder-action-buttons'])}}
                {{ Form::close() }}
                <a href="{{route('files.edit', $file->id)}}" class="submit-button html-button html-button-left folder-action-buttons">edit</a>
            @endif
            </div>
            <div class="col-md-12">
                <hr class="create-hr-upper">
            </div>
        </div>
        @if($file->file != NULL)
        <div class="row">
            <div class="col-md-12">
                <div class="embed-responsive embed-responsive-4by3">
                    <object data="{{asset('folder_files/' . $file->file)}}" width="100%" type="application/pdf"></iframe>
                </div>
            </div>
        </div>
        @endif
@endsection

@section('scripts')

    {{Html::script('js/parsley.min.js')}}

@endsection