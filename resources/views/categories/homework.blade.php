@extends('layouts/main')

@section('title', '- Homework')

@section('styles')

    {{Html::style('css/styles.css')}}
    
@endsection

@section('content')

    <div class="input-container input-container-posts">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-8">
                <h1 class="all-form-group">Homework</h1>
            </div>
            <div class="col-md-4 col-sm-4 col-4 top-button">
                <a href="{{route('questions.create')}}" class="submit-button posts-ask-question-button">Ask question</a>
            </div>
            <div class="col-md-12">
                <hr class="create-hr-upper">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 mx-auto">
                <div class="text-center category-box">
                    <a href="{{route('categories.homework')}}" class="all-form-group categories-button">Homework</a><br>
                </div>
            </div>
            <div class="col-md-4 mx-auto">
                <div class="text-center category-box">              
                    <a href="{{route('categories.programming')}}" class="all-form-group categories-button">Programming</a><br>
                </div>
            </div>
            <div class="col-md-4 mx-auto">
                <div class="text-center category-box">
                    <a href="{{route('categories.lecture')}}" class="all-form-group categories-button">Lecture</a><br>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table all-form-group">
                    <thead class="thead-styling">
                        <th>title</th>
                        <th>description</th>
                        <th class="table-category">comments</th>
                        <th class="table-time">last modified</th>
                        <th></th>
                    </thead>

                    <tbody>
                        @foreach ($posts->where('category_id', '1') as $post)
                            <tr>
                                <td class="table-styling">{{$post->title}}</td>
                                <td class="table-styling">{{substr($post->body, 0, 10)}} {{strlen($post -> body) > 50 ? "..." : ""}}</td>
                                <td class="table-styling table-category">{{$post->comments->count()}}</td>
                                <td style="text-transform: lowercase; table-styling" class="table-time">{{date('M j, Y', strtotime($post -> updated_at))}}</td>
                                <td>
                                    <a href="{{route('questions.show', $post -> id)}}" class="submit-button html-button">View</a>
                                    @if((Auth::user()->id == $post->user->id) || (Auth::user()->isAdmin == 1))
                                    <a href="{{route('questions.edit', $post -> id)}}" class="submit-button html-button html-button-left">Edit</a>
                                    @endif
                                </td>
                            </tr>  
                        @endforeach
                    </tbody>
                </table>
                <div class="pagination">
                    {{$posts -> links("pagination::bootstrap-4"), ['class' => 'pagination']}}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    {{Html::script('js/parsley.min.js')}}

@endsection