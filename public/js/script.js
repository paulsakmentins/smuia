    $('#program').change(function(e){
        if($(this).val() == "Lecturer"){
            $("#year option[value='1 year']").prop('disabled',true);
            $("#year option[value='2 year']").prop('disabled',true);
            $("#year option[value='3 year']").prop('disabled',true);
            $("#year option[value='4 year']").prop('disabled',true);
        }
        else if ($(this).val() == "Magister"){
            $("#year option[value='3 year']").prop('disabled',true);
            $("#year option[value='4 year']").prop('disabled',true);
        }
        else {
            $("#year option[value='1 year']").prop('disabled',false);
            $("#year option[value='2 year']").prop('disabled',false);
            $("#year option[value='3 year']").prop('disabled',false);
            $("#year option[value='4 year']").prop('disabled',false);
            }
        });