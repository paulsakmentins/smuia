<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 

    Auth::routes();

    Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');

    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    
    Route::get('password/reset/{token?}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );

    Route::get('login/resend', 'Auth\ActivationResendController@verificationMail')->name('login.verify');

    Route::post('resend', 'Auth\ActivationResendController@resend')->name('login.resend');

    Route::get('about', 'PagesController@getAbout');

    Route::get('contact', 'PagesController@getContact');

    Route::post('contact', 'PagesController@postContact');

    Route::get('/', 'PagesController@getIndex')->middleware('auth');

    Route::resource('questions', 'PostController');

    Route::get('questions/categories/homework', 'PostController@getHomework')->name('categories.homework');

    Route::get('questions/categories/programming', 'PostController@getProgramming')->name('categories.programming');

    Route::get('questions/categories/lecture', 'PostController@getLecture')->name('categories.lecture');
    
    Route::get('profile', 'UserController@getProfile')->name('account.profile');
    
    Route::post('profile', 'UserController@getPicture')->name('profile.update');

    Route::get('user/{id}', 'UserController@getUser')->name('account.user');

    Route::post('user/{id}/update', 'UserController@updateUser')->name('account.update');

    Route::delete('user/{id}/destroy', 'UserController@destroy')->name('account.destroy');

    Route::get('users', 'UserController@getUsers')->name('account.users');
    
    Route::post('profile', 'UserController@update')->name('profile.update');

    Route::get('profile/{id}', 'UserController@show')->name('account.show');

    Route::get('archive', 'FolderController@getPage')->name('folders.page')->middleware('lecturer');
    
    Route::get('archive/course/{folder_id}', 'FolderController@show')->name('folders.show')->middleware('lecturer');

    Route::get('archive/{program_id}', 'FolderController@index')->name('folders.index')->middleware('lecturer');

    Route::get('archive/course/{program_id}/create', 'FolderController@create')->name('folders.create')->middleware('lecturer');
    
    Route::get('archive/course/{folder_id}/edit', 'FolderController@edit')->name('folders.edit')->middleware('lecturer');
    
    Route::post('folders/{program_id}', 'FolderController@store')->name('folders.store')->middleware('lecturer');

    Route::post('archive/course/{folder_id}', 'FolderController@update')->name('folders.update')->middleware('lecturer');

    Route::delete('archive/course/{folder_id}', 'FolderController@destroy')->name('folders.destroy')->middleware('lecturer');
    
    Route::delete('archive/course/folder/{file_id}', 'FilesController@destroy')->name('files.destroy')->middleware('lecturer');
    
    Route::post('files/{folder_id}', 'FilesController@store')->name('files.store')->middleware('lecturer');

    Route::get('archive/course/folder/{file_id}', 'FilesController@show')->name('files.show')->middleware('lecturer');

    Route::get('archive/course/{folder_id}/files/create', 'FilesController@create')->name('files.create')->middleware('lecturer');

    Route::get('archive/course/folder/{file_id}/edit', 'FilesController@edit')->name('files.edit')->middleware('lecturer');
    
    Route::post('archive/course/folder/{file_id}', 'FilesController@update')->name('files.update')->middleware('lecturer');
    
    Route::post('comments/{post_id}', ['uses' => 'CommentsController@store', 'as' => 'comments.store']);
    
    Route::put('comments/{id}', ['uses' => 'CommentsController@update', 'as' => 'comments.update']);

    Route::get('comments/{id}/edit', ['uses' => 'CommentsController@edit', 'as' => 'comments.edit']);

    Route::delete('comments/{id}', ['uses' => 'CommentsController@destroy', 'as' => 'comments.destroy']);