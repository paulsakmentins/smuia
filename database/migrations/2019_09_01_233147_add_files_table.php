<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->string('file');
            $table->integer('user_id')->unsigned();
            $table->integer('folder_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('files', function($table)
        {
             $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
             $table->foreign('folder_id')->references('id')->on('folders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
        Schema::table('files', function (Blueprint $table) {
            $table->dropForeign(['user_id', 'folder_id']);
        });
    }
}
