<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = 'programs';

    public function folder()
    {
       return $this->hasMany('App\Folder');
    }
}
