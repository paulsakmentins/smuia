<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    public function program() 
    {
        return $this->belongsTo('App\Program');
    }

    public function files()
    {
        return $this->hasMany('App\File');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
