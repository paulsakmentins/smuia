<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;

class Lecturer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->program == 'Lecturer')
        {
            return redirect('/')->withWarning('You are not allowed to access files');
        }
        return $next($request);
    }
}
