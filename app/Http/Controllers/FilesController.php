<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Program;
use App\Folder;
use App\File;
use Storage;
use Session;
use Image;
use Auth;

class FilesController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($id)
    {
        $files = File::find($id);
        $folder = Folder::find($id);
        $program = Program::find($id);

        return view('files.index')->withFiles($files)->withFolder($folder)->withProgram($program);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $folder = Folder::find($id);
        $program = Program::find($id);
        return view('files.create')->withFolder($folder);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $folder_id)
    {
        $this->validate($request, [
            'name' => 'required|min:1|max:50',
            'file' => 'required|max:10000',
            ],
            $errors = [
                'max' => 'Maximum size of image is 10MB'
            ]
        );

        $folder = Folder::find($folder_id);

        $file = new File();
            
        $file->name = $request->name;
        $file->folder()->associate($folder);
        $file->user_id = auth()->user()->id;

        //file save
        if($request->hasFile('file')) {
            $content = $request->file('file');
            $filename = time() . "." . $content->getClientOriginalExtension();
            $location = public_path('folder_files/');
            $content->move($location, $filename);
            $file->file = $filename; 
        }

        $file->save();

        Session::flash('success', 'Your file was successfully added!');

        return redirect()->route('folders.show',  $folder->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $file = File::find($id);
        return view('files.show')->withFile($file);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $file = File::find($id);
        if((Auth::user()->id == $file->user->id) || (Auth::user()->isAdmin == 1))
        {
            return view('files.edit')->withFile($file);
        }
        else
        {
            Session::flash('warning', 'You can not edit other user\'s file');
            return redirect()->route('files.show', $file->id);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file = File::find($id);

        $this->validate($request, array(
            'name' => 'required|min:1|max:50',
            'file' => 'max:10000|mimes:jpeg, jpg, png'
        ));

        $file->name = $request->name;

        if($request->hasFile('file')) {
            $content = $request->file('file');
            $filename = time() . "." . $content->getClientOriginalExtension();
            $location = public_path('folder_files/');
            $content->move($location, $filename);
            $oldFileName = $file->content;
            $file->file = $filename;
            Storage::delete($oldFileName);
        }

        $file->save();

        Session::flash('success', 'Your file was successfully edited!');

        //redirect with flash data
        return redirect()->route('files.show', $file->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = File::find($id);
        if((Auth::user()->id == $file->user->id) || (Auth::user()->isAdmin == 1))
        {
            if($file->file)
            {
                $image_path = public_path('folder_files/') . $file->file;
                unlink($image_path);
            }
                
            $user_id = $file->user->id;
            $folder_id = $file->folder->id;

            $file->delete();

            Session::flash('success', "Your file was successfully deleted");

            return redirect()->route('folders.show', $folder_id);
        }
        else
        {
            Session::flash('warning', 'You can not delete other user\'s file');
            return redirect()->route('files.show', $file->id);
        }
    }
}
