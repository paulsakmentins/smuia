<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\User;
use Session;
use Image;
use Storage;
use File;
use Auth;

class PostController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getHomework() { 

        $posts = Post::orderBy('updated_at', 'desc') -> paginate(10);
        $categories = Category::all();

        return view('categories.homework')->withPosts($posts)->withCategories($categories);
    }

    public function getProgramming() { 

        $posts = Post::orderBy('updated_at', 'desc') -> paginate(10);
        $categories = Category::all();

        return view('categories.programming')->withPosts($posts)->withCategories($categories);
    }

    public function getLecture() { 

        $posts = Post::orderBy('updated_at', 'desc') -> paginate(10);
        $categories = Category::all();

        return view('categories.lecture')->withPosts($posts)->withCategories($categories);
    }

    public function getQuestions() {
        $posts = Post::orderBy('updated_at', 'desc') -> paginate(10);
        $categories = Category::all();

        return view('questions.profile')->withPosts($posts)->withCategories($categories);
    }

    
    public function index()
    {

        //create a variable
        $posts = Post::orderBy('created_at', 'desc') -> paginate(10);
        $categories = Category::all();

        //return a view with variable
        return view('questions.index')->withPosts($posts)->withCategories($categories);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('questions.create')->withCategories($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validating input
            $this->validate($request, array(
                'title' => 'required|max:255',
                'body' => 'required',
                'category_id' => 'required|integer',
                'image' => 'sometimes|image',
            ));

        //store
            $post = new Post;
            
            $post->title = $request->title;
            $post->body = $request->body;
            $post->category_id = $request->category_id;
            $post->user_id = auth()->user()->id;

            //image save
            if($request->hasFile('image')) {
                $image = $request->file('image');
                $filename = time() . "." . $image->getClientOriginalExtension();
                $location = public_path('post_images/' . $filename);
                Image::make($image)->resize(800, 400)->save($location);

                $post->image = $filename; 
            }

            $post -> save();

            Session::flash('success', 'Your question was successfully added!');

        //redirect
            return redirect() -> route('questions.show', $post->id);
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $categories = Category::all();
        $user = User::find($id);

        return view('questions.show')->withPost($post)->withUser($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //find the question
            $post = Post::find($id);
            $categories = Category::all();
            
        if((Auth::user()->id == $post->user->id) || (Auth::user()->isAdmin == 1))
        {
            return view('questions.edit')->withPost($post)->withCategories($categories);
        }
        else 
        {
            Session::flash('warning', 'You can not delete other user\'s post');
            return redirect()->route('questions.show', $post->id);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validating changes
        $post = Post::find($id);

            $this->validate($request, array(
                'title' => 'required|max:255',
                'body' => 'required',
                'category_id' => 'required|integer',
                'image' => 'image'
            ));

        //store
        $post = Post::find($id);
            
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->category_id = $request -> input('category_id');

        if($request->hasFile('image')) {
            //Add new image
            $image = $request->file('image');
            $filename = time() . "." . $image->getClientOriginalExtension();
            $location = public_path('post_images/' . $filename);
            Image::make($image)->resize(800, 400)->save($location);
            $oldfilename = $post->image;
            //Update database
            $post->image = $filename;
            //Delete old image
            Storage::delete($oldfilename);
        }

        $post -> save();

        //success message
        Session::flash('success', 'Your question was successfully edited!');

        //redirect with flash data
        return redirect()->route('questions.show', $post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //delete
            $post = Post::find($id);

        if((Auth::user()->id == $post->user->id) || (Auth::user()->isAdmin == 1))
        {
            if($post->image)
            {
                $image_path = public_path('post_images/') . $post->image;
                unlink($image_path);
            }
            
            $post->delete();

        //success message
            Session::flash('success', "Your question was successfully deleted");
        //redirect with flash data
            return redirect() -> route('questions.index');
        }
        else
        {
            Session::flash('warning', 'You can not delete other user\'s post');
            return redirect()->route('questions.show', $post->id);
        }
    }

}
