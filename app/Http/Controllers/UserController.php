<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use Storage;
use App\Post;
use App\User;
use Session;

class UserController extends Controller
{
    public function __construct() {
        $this -> middleware('auth');
    }

    public function getProfile() {

        $posts = Post::orderBy('updated_at', 'desc') -> paginate(10);
        $user = User::all();

        return view('account.profile', array('user' => Auth::user()))->withUser($user);
    }

    public function getPicture(Request $request) {
        $posts = Post::orderBy('updated_at', 'desc') -> paginate(10);
        $user = User::all();


         if($request->hasFile('image')) {
             $image = $request->file('image');
             $filename = time() . '.' . $image->getClientOriginalExtension();
             Image::make($image)->resize(300, 300)->save(public_path('profile_images/' . $filename));

             $user = Auth::user();
             $user->image = $filename;
             $user->save();
         }
         else {

         }

         return view('account.profile', array('user' => Auth::user()))->withPosts($posts)->withUser($user);
    }

    public function update(Request $request) {
        $user = Auth::user();

        $data = $this->validate($request, [
            'name' => 'required|max:255|min:3',
            'surname' => 'required|max:255|min:3',
            'program' => 'required',
            'year' => 'sometimes',
        ]);

        $user->name = $data['name'];
        $user->surname = $data['surname'];
        $user->program = $data['program'];
        $user->year = isset($data['year']) ? $data['year'] : null;

        if($request->hasFile('image')) {
            //Add new image
            $image = $request->file('image');
            $filename = time() . "." . $image->getClientOriginalExtension();
            $location = public_path('profile_images/' . $filename);
            Image::make($image)->resize(300, 300)->save($location);
            $oldfilename = $user->image;
            //Update database
            $user->image = $filename;
            //Delete old image
            Storage::delete($oldfilename);
        }

        $user->save();
        return redirect('profile')->with('success', 'Profile has been updated!');
    }

    public function getUsers() {

        if(Auth::user()->isAdmin == 1)
        {
        $users = User::orderBy('created_at', 'desc')->paginate(10);
        return view('account.users')->withUsers($users);
        }
        else
        {
            Session::flash('warning', 'You can not manage users');
            return redirect('/profile');
        }
    }

    public function getUser(Request $request, $id) {

        if(Auth::user()->isAdmin == 1)
        {
        $user = User::find($id);
        return view('account.user')->withUser($user);
        }
        else 
        {
            Session::flash('warning', 'You can not manage users');
            return redirect('/profile');
        }
    }

    public function updateUser(Request $request, $id){

        if(Auth::user()->isAdmin == 1)
        {
            $user = User::find($id);

            $data = $this->validate($request, [
                'name' => 'required|max:255|min:3',
                'surname' => 'required|max:255|min:3',
                'program' => 'required',
                'year' => 'sometimes',
            ]);

            $user->name = $data['name'];
            $user->surname = $data['surname'];
            $user->program = $data['program'];
            $user->year = isset($data['year']) ? $data['year'] : null;

            if($request->hasFile('image')) {
                //Add new image
                $image = $request->file('image');
                $filename = time() . "." . $image->getClientOriginalExtension();
                $location = public_path('profile_images/' . $filename);
                Image::make($image)->resize(300, 300)->save($location);
                $oldfilename = $user->image;
                //Update database
                $user->image = $filename;
                //Delete old image
                Storage::delete($oldfilename);
            }
            $user->save();
            
            return redirect()->route('account.user', $user->id)->with('success', 'Profile has been updated!');
        }
        else 
        {
            Session::flash('warning', 'You can not manage users');
            return redirect('/profile');
        }
    }

    public function show($id) {
        $user = User::find($id);
        $posts = Post::orderBy('updated_at', 'desc') -> paginate(10);

        return view('account.show')->withPosts($posts)->withUser($user);
    }

    public function destroy($id)
    {
        if(Auth::user()->isAdmin == 1)
        {
            $user = User::find($id);

            if($user->image)
            {
                $image_path = public_path('profile_images').'/'.$user->image;
                unlink($image_path);
            }

            $user->delete();

            Session::flash('success', "User was successfully deleted");

            return redirect('/users');
        }
        else
        {
            Session::flash('warning', 'You can not manage users');
            return redirect('/profile');
        }
    }
}
