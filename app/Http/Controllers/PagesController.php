<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Requests;
use Mail;
use Session;

class PagesController extends Controller {

    public function getIndex() {
        return view('pages.welcome');
    } 

    public function getAbout () {
        return view('pages.about');
    }

    public function getContact () { 
        return view('pages.contact');
    }

    public function postContact(Request $request)  {
        $this->validate($request, [
            'email' => 'required|email',
            'subject' => 'required|min:3',
            'message' => 'min:5']);

            $data = array(
                'email' => $request->email,
                'subject' => $request->subject,
                'body' => $request->message 
            );

        Mail::send('emails.contact', $data, function($message) use ($data){
            $message->from($data['email']);
            $message->to('smuiauol@gmail.com');
            $message->subject('SMUIA contact form');
        });

    $status='ok';
    return redirect('/contact')->withStatus($status);
    }
}