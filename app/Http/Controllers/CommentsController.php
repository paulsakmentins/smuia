<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Comment;
use Session;
use Auth;

class CommentsController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $post_id)
    {
        $this->validate($request, array(
            'comment' => 'required|min:5|max:2000'
        ));

        $post = Post::find($post_id);

        $comment = new Comment();
        $comment->comment = $request->comment;
        $comment->approved = true;
        $comment->post()->associate($post);
        $comment->user_id = auth()->user()->id;
        
        $comment->save();
        
        Session::flash("success", "Comment was added");

        return redirect() -> route('questions.show', $post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = Comment::find($id);

        if((Auth::user()->id == $comment->user->id) || (Auth::user()->isAdmin == 1))
        {
            return view('comments.edit')->withComment($comment);
        }
        else
        {
            Session::flash('warning', 'You can not edit other user\'s comment');
            return redirect()->route('questions.show', $comment->post->id);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment = Comment::find($id);

        $this->validate($request, array(
            'comment' => 'required|min:5'
        ));

        $comment->comment = $request->comment;
        $comment->save();

        Session::flash('success', 'Comment successfully updated');

        return redirect()->route('questions.show', $comment->post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //delete
        $comment = Comment::find($id);

        if((Auth::user()->id == $comment->user->id) || (Auth::user()->isAdmin == 1))
        {
            $post_id = $comment->post->id;
            $user_id = $comment->user->id;

            $comment -> delete();

        //success message
            Session::flash('success', "Your comment was successfully deleted");
        //redirect with flash data
            return redirect()->route('questions.show', $post_id);
        }
        else
        {
            Session::flash('warning', 'You can not delete other user\'s comment');
            return redirect()->route('questions.show', $comment->post->id);
        }
    }
}
