<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Mail\VerifyMail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\User;
use App\VerifyUser;
use App\Program;
use Auth;
use Image;
use Storage;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'email' => 'required|email|max:255|regex:/(.*)@(.*)lu\.lv/i|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        $user = User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'program' => $data['program'],
            'year' => isset($data['year']) ? $data['year'] : null,
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);

        

        Mail::to($user->email)->send(new VerifyMail($user));

        return $user;
    }

    public function showRegistrationForm()
{
    $program = Program::all();

    return view('auth.register')->withProgram($program);
}

    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser)){
            $user = $verifyUser->user;
            if(!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->save();
                $status = "Your account is verified, you may log in now";
            }
            else {
                $status = "Your account is verified, you may log in now";
            }
        }
        else {
            return redirect('login')->with('warning', 'Your e-mail does not match our records');
        }

    return redirect('login')->with('status', $status);
    }

    public function registered(Request $request, $user)
    {
        $this->guard()->logout();
        return redirect('login')->with('status', 'We have sent you an account activation link, please check your e-mail for more details.');
    }
}
