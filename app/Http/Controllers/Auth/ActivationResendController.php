<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Mail\VerifyMail;
use App\VerifyUser;
use Mail;

class ActivationResendController extends Controller
{
    public function verificationMail()
    {
        return view('auth.verify');
    }

    public function resend(Request $request)
    {
        $this->validate($request, array(
            'email' => 'required|email|max:255|regex:/(.*)@(.*)lu\.lv/i'
        ));

        $user = User::where('email', $request->email)->first();

        if($user == null)
        {
            return redirect()->route('login')->withDanger('Your e-mail does not match our records');
        }
        if($user->verified != 1)
        {
            Mail::to($user->email)->send(new VerifyMail($user));
        
            return redirect()->route('login')->withStatus('We sent you an account activation link, please check your e-mail for more details');
        }
    return redirect()->route('login')->withStatus('Your account is already verified');
    }
}
