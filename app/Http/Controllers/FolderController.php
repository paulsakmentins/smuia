<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Folder;
use App\Program;
use App\File;
use Session;
use Auth;

class FolderController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function getPage() 
    {
        $program = Program::all();

        $bachelors = $program->intersect(Program::whereIn('id', [1, 2, 3, 4])->get());
        $magisters = $program->intersect(Program::whereIn('id', [5, 6])->get());
        return view('folders.page')->withBachelors($bachelors)->withMagisters($magisters);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $folders = Folder::all();
        $program = Program::find($id);

        return view('folders.index')->withFolders($folders)->withProgram($program);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $program = Program::find($id);
        return view('folders.create')->withProgram($program);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $program_id)
    {
        $this->validate($request, array(
            'name' => 'required|min:5|max:30'
        ));

        $program = Program::find($program_id);

        $folder = new Folder();

        $folder->name = $request->name;
        $folder->program()->associate($program);
        $folder->user_id = auth()->user()->id;

        $folder->save();

        Session::flash('success', 'New folder added!');

        return redirect()->route('folders.index', $program->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $file = File::all();
        $folder = Folder::find($id);
        $program = Program::find($id);
        return view('folders.show')->withFolder($folder)->withFiles($file)->withProgram($program);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $folder = Folder::find($id);
        if((Auth::user()->id == $folder->user->id) || (Auth::user()->isAdmin == 1))
        {
            return view('folders.edit')->withFolder($folder);
        }
        else
        {
            Session::flash('warning', 'You can not edit other user\'s folder');
            return redirect()->route('folders.show', $folder->id);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $folder = Folder::find($id);

        $this->validate($request, array(
            'name' => 'required|min:5|max:30'
        ));

        $folder->name = $request->name;

        $folder->save();

        Session::flash('success', 'Your folder was successfully edited!');

        return redirect()->route('folders.show', $folder->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $folder = Folder::find($id);
        if((Auth::user()->id == $folder->user->id) || (Auth::user()->isAdmin == 1))
        {
            $program_id = $folder->program->id;
            $user_id = $folder->user->id;

            $folder->delete();

            Session::flash('success', "Your folder was successfully deleted");

            return redirect()->route('folders.index', $program_id);
        }
        else
        {
            Session::flash('warning', 'You can not edit other user\'s folder');
            return redirect()->route('folders.show', $folder->id);
        }
    }
}
